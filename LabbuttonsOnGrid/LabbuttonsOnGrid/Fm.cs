﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabbuttonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;
        private int minRows = 1; // Minimum control
        private int minCols = 2;

        private int maxRows = 10; // Maximum control
        private int maxCols = 10;

        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;

        public Fm()
        {
            InitializeComponent();

            CreateButtons();

            ReSizeButtons();
            this.Resize += (s, e) => ReSizeButtons();
            this.KeyPreview = true;
            this.KeyDown += Fmkey;
        }

        private void Fmkey(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Add:
                    if (e.Shift) // Adds delets with or not Shift and +(-)
                    {
                        Rows++;
                        Renewal();
                    }
                    else
                    {
                        Cols++;
                        Renewal();
                    }
                    break;
                case Keys.Subtract:
                    if (e.Shift)
                    {
                        Rows--;
                        Renewal();
                    }
                    else
                    {
                        Cols--;
                        Renewal();
                    }
                    break;
                default:
                    break;
            }
        }

        private void ReSizeButtons()
        {
            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = this.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)

                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, i * xCellHeight); // X, Y

                }
        }
        // HW : отказаться от "int n = 1"
        // Менять количество рядов и количество столбцов, например, с помощью кнопок формы или клавиш клавиатуры
        // Добавить ограничение на Rows и Cols
        // Менять фон кнопки с MouseEnter при наведении (2 универс. метода из sender.sln)

        private void CreateButtons()
        {
           // int n = 1;
          bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                 
                    bu[i, j] = new Button();
                    bu[i, j].Text = $"{i * Cols + 1 + j}"; // Numerous instead of "int n"
                    bu[i, j].MouseEnter += buEnter;
                    bu[i, j].MouseLeave += buLeave;

                    this.Controls.Add(bu[i, j]);
                }
        }

        private void buLeave(object sender, EventArgs e)
        {
            if (sender is Button x)
            {
                x.BackColor = SystemColors.Control;
            }
        }
        private void buEnter(object sender, EventArgs e)
        {
            if (sender is Button x)
            {
                x.BackColor = Color.YellowGreen;
            }
        }
        private void DeleteButtons()
        {
            this.Controls.Clear();
        }
        private int Trim(int value, int min, int max) // for string removing set characters
            {
                value = value > max ? min : value;
                value = value > max ? min : value;
                return value;
            }
        private void Renewal()
        {
            Rows = Trim(Rows, minRows, maxRows);
            Cols = Trim(Cols, minCols, maxCols);
            bu = new Button[Rows, Cols];
            DeleteButtons();
            CreateButtons();
            ReSizeButtons();
        }


    }
}
