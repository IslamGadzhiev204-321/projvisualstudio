﻿using System;
using System.Collections.Generic;

namespace LabDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>();
            x.Add(10, "Тула");
            x.Add(20, "Брянск");
            x.Add(30, "Калуга");
            x.Add(40, "Орёл");
            x.Add(50, "Самара");
            x.Add(60, "Полтава");
            x.Add(70, "Псков");
            x.Add(80, "Красногорск");

            x[60] = "Москва";
            x.Remove(50);

            Console.WriteLine(x[30]);
            Console.WriteLine("---");

           // foreach (KeyValuePair<int, string> v in x)
            foreach (var v in x)
            {
                Console.WriteLine($"Key={v.Key}, Value={v.Value}");
            }
        }
    }
}
