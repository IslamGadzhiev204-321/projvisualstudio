﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabFileExplorer
{
    public partial class Fm : Form
    {
        public string CurDir { get; private set; }
        public string Selitem { get; private set; }

        public Fm()
        
        {
            InitializeComponent();
            //CurDir = "C:/" ;
            CurDir = Directory.GetCurrentDirectory();
            //buBack.Click += 
            //buForward +=
            buUp.Click += (s, e) => LoadDir(Directory.GetParent(CurDir).ToString());
            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;
            miViewLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miViewSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miViewList.Click += (s, e) => lv.View = View.List;
            miViewDetails.Click += (s, e) => lv.View = View.Details;
            miViewTile.Click += (s, e) => lv.View = View.Tile;
            LoadDir(CurDir);
            lv.ItemSelectionChanged += (s, e) => Selitem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s, e) => LoadDir(Selitem);
            this.Text += " : Drivers= " + string.Join(" ", Directory.GetLogicalDrives());

            // HW:
            // Проверкка на buUp
            // Добавить механизм на кнопки back и forward, например, через стэк
            // Добавить различные иконки к определённым файлам - item.Extension
            // Добавить панель - компоненты динамические - кнопки с дисками (иконка + тект [на отдельной панели Directory.GetLogicalDrives()] )
            // По кнопке backspace также как и ^ перейти на уровень выше (buUp)
            // Добавить механизм открытия файла (например, отдельная форма. текст или картинка, или показать путь к файлу)
            // Добавить кнопку "Создать папку" - САМОСТОЯТЕЛЬНО !
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK )
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadDir(edDir.Text);
            }
        }

        private void LoadDir(string NewDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(NewDir);
            lv.BeginUpdate();
            lv.Items.Clear();

            foreach (var item in directoryInfo.GetDirectories())
            {
                lv.Items.Add(item.Name, 0);
            }
            foreach (var item in directoryInfo.GetFiles())
            {
                lv.Items.Add(item.Name, 1);
              //  item.Extension
            }

            lv.EndUpdate();
            CurDir = NewDir;
            edDir.Text = NewDir;
        }
    }
}
