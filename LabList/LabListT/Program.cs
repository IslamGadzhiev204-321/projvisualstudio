﻿using System;
using System.Collections.Generic;

namespace LabListT
{
    class Program
    {
        static void Main(string[] args)
        {
            //  int[] x = new int[] { 1, 2, 3, 4};
            List<int> x = new List<int>() { 1, 2, 3, 4 };
            x.Add(5);
            x.AddRange(new int[] { 6, 7, 8 });
            x.Insert(0, 9); // 0 - позиция, объект
            x.RemoveAt(2); // удаление 2-го объекта
            foreach (var i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
