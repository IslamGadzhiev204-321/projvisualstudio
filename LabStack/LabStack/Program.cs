﻿using System;
using System.Collections;

namespace LabStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack x = new Stack();
            x.Push("This is Stack lab");
            x.Push(777);
            x.Push("Key");

            Console.WriteLine(x.Peek()); // посмотреть
            Console.WriteLine("---");

            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Возможно что-то пошло не так : {ex.Message}");
            }
            // HW:
            // LabStackT
        }
    }
}
