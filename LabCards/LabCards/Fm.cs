﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabCards
{
    public partial class FM : Form
    {
        private const int cardCount = 10; // Cards amount on the screen
        private readonly Bitmap b;
        private readonly Graphics g;
      private readonly ImageBox imageBox; //  Pictures collection
        private readonly CardPack cardPack; // Card deck

        public FM()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            imageBox = new ImageBox(Properties.Resources.Cards1, 4, 13);

            cardPack = new CardPack(imageBox.Count);
            

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            this.Click += FM_Click;

          //imageBox[5];
        }

        private void FM_Click(object sender, EventArgs e)
        {
            // g.DrawImage(imageBox[3], 0, 0);
           // cardPack.CardRandom(); // makes pictures without order pack
            Random rnd = new Random();
            g.Clear(SystemColors.Control);

            for (int i = 0; i < cardCount; i++)
            {
                g.DrawImage(imageBox[cardPack[i]], 
                    rnd.Next(this.ClientSize.Width),
                    rnd.Next(this.ClientSize.Height));
            }
            this.Invalidate();
        }
    }
}

// HW:
// Make different class for storage each player cards index  =>
// => (cards adds)
// With F2 - draw cards fan with image rotate in different buffer
// With F3 - draw cards in random order
// Make cards fan

// move by mouse click and move
// Make cards fan change size by mouse wheel
