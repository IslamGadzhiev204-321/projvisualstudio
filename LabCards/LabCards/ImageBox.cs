﻿using System;
using System.ComponentModel.Design;
using System.Drawing;

namespace LabCards
{
    internal class ImageBox
    {
        private Bitmap[] images;

        public int Rows { get; }
        public int Cols { get; }
        public int Count { get; }

        public ImageBox(object image, int rows, int cols) : this(image, rows, cols, rows*cols) { }

        public ImageBox(object image, int rows, int cols, int count)
        {
            Rows = rows;
            Cols = cols;
            Count = count;
            LoadImages((Bitmap)image);

        }

        public Bitmap this[int index] { get { return images[index]; } } // индексатор

        private void LoadImages(Bitmap image)
        {
            images = new Bitmap[Count];
            var w = image.Width / Cols;
            var h = image.Width / Rows;
            var n = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols && n < Count ; j++, n++)
                { 
                  images[n] = new Bitmap(w, h);
                    // (1)
                    var g = Graphics.FromImage(images[n]);
                    g.DrawImage(image, 0, 0, new Rectangle(j *w, i * h, w, h), GraphicsUnit.Pixel);
                    g.Dispose();
                    // (2)
                   // using (var g =  Graphics.FromImage(images[n]))
                  //  { 
                   //     g.DrawImage.
                   // }
                    //
                }
            }
        }
    }
}