﻿
namespace Example
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fm));
            this.Restbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Restbutton
            // 
            this.Restbutton.BackColor = System.Drawing.Color.MediumVioletRed;
            this.Restbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Restbutton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Restbutton.Location = new System.Drawing.Point(563, 12);
            this.Restbutton.Name = "Restbutton";
            this.Restbutton.Size = new System.Drawing.Size(87, 23);
            this.Restbutton.TabIndex = 0;
            this.Restbutton.Text = "Restart";
            this.Restbutton.UseVisualStyleBackColor = false;
            this.Restbutton.Click += new System.EventHandler(this.Restbutton_Click_1);
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.Restbutton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Fm";
            this.Text = "Chess";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Restbutton;
    }
}

