﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        // private readonly Bitmap imBG; // Фон доски
        private readonly Bitmap imCS; // Спрайты фигур
        public Image ChessSprites;
        public Button prevButton;
        public Boolean Moving;
        public int CurrentPlayer;

        public int[,] map = new int[8, 8] // Игровая матрица фигур
       {
         {15, 14, 13, 12, 11, 13, 14, 15 },
         {16, 16, 16, 16, 16, 16, 16, 16 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {26, 26, 26, 26, 26, 26, 26, 26 },
         {25, 24, 23, 22, 21, 23, 24, 25 },
       };
        public Button[,] butts = new Button[8, 8];

        public Fm()
        {
            InitializeComponent();

            // imBG = Properties.Resources.Chessboard;
            // this.Height = imBG.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.Width = 700;
            this.Height = 550;

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { e.Graphics.DrawImage(b, 0, 0); };
            // g.DrawImage(imBG, 0, 0); // Задаём фон шахматной доски

            imCS = Chess___project.Properties.Resources.Chesssprites;
            ChessSprites = new Bitmap(Chess___project.Properties.Resources.Chesssprites);
            Image part = new Bitmap(64, 64); // нормализация
            Graphics g2 = Graphics.FromImage(part);
            g2.DrawImage(ChessSprites, new Rectangle(0, 0, 64, 64), 0, 0, 150, 150, GraphicsUnit.Pixel);

            // panel1.BackColor = Color.FromArgb(0, SystemColors.Control); // Для обеспечения buttons прозрачными (сквозь панель) ??
            Init();
        }

        public void Init()
        {
            map = new int[8, 8]
            {
         {15, 14, 13, 12, 11, 13, 14, 15 },
         {16, 16, 16, 16, 16, 16, 16, 16 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {0, 0, 0, 0, 0, 0, 0, 0 },
         {26, 26, 26, 26, 26, 26, 26, 26 },
         {25, 24, 23, 22, 21, 23, 24, 25 },
            };

            CurrentPlayer = 1;
            CreateMap();
            CloseSteps();
        }
        public void CreateMap()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    butts[i, j] = new Button();
                    Button butt = new Button();
                    Button Restbutton = new Button();
                    butt.Size = new Size(64, 64);
                    butt.Location = new Point(j * 64, i * 64);

                    switch (map[i, j] / 10) // проверка номера игрока
                    {
                        case 1:
                            Image part = new Bitmap(64, 64);
                            Graphics g2 = Graphics.FromImage(part);
                            g2.DrawImage(ChessSprites, new Rectangle(0, 0, 64, 64), 0 + 150 * (map[i, j] % 10 - 1), 0, 150, 150, GraphicsUnit.Pixel);
                            butt.BackgroundImage = part;
                            break;

                        case 2:
                            Image part1 = new Bitmap(64, 64);
                            Graphics g3 = Graphics.FromImage(part1);
                            g3.DrawImage(ChessSprites, new Rectangle(0, 0, 64, 64), 0 + 150 * (map[i, j] % 10 - 1), 150, 150, 150, GraphicsUnit.Pixel);
                            butt.BackgroundImage = part1;
                            break;

                    }
                    butt.BackColor = Color.White;
                    butt.Click += new EventHandler(PressOnFigures);
                    Restbutton.Click += new EventHandler(Restbutton_Click_1);
                    this.Controls.Add(butt);
                    butts[i, j] = butt;
                }
            }
        }
        public void PressOnFigures(object sender, EventArgs e)
        {
            Button pressedButton = sender as Button;
            if (prevButton != null)
            {
                prevButton.BackColor = Color.White;
            }

            if (map[pressedButton.Location.Y / 64, pressedButton.Location.X / 64] != 0 &&
                map[pressedButton.Location.Y / 64, pressedButton.Location.X / 64] / 10 == CurrentPlayer) // Принадлежит текущему игроку
            {
                CloseSteps();
                pressedButton.BackColor = Color.Purple;
                DeactivateAllButtons();
                pressedButton.Enabled = true;
                ShowSteps(pressedButton.Location.Y / 64, pressedButton.Location.X / 64,
                    map[pressedButton.Location.Y / 64, pressedButton.Location.X / 64]);

                if (Moving)
                {
                    CloseSteps();
                    pressedButton.BackColor = Color.Purple;
                    ActivateAllButtons();
                    Moving = false;
                }
                else
                    Moving = true;
            }
            else
            {
                if (Moving)
                {
                    int temp = map[pressedButton.Location.Y / 64, pressedButton.Location.X / 64];
                    map[pressedButton.Location.Y / 64, pressedButton.Location.X / 64] = map[prevButton.Location.Y / 64, prevButton.Location.X / 64];
                    map[prevButton.Location.Y / 64, prevButton.Location.X / 64] = temp;
                    pressedButton.BackgroundImage = prevButton.BackgroundImage;
                    prevButton.BackgroundImage = null;
                    Moving = false;

                    CloseSteps();
                    ActivateAllButtons();
                    SwitchPlayer();
                }
                pressedButton.BackColor = Color.Purple;
            }

            prevButton = pressedButton;
        }
        public void ShowSteps(int IcurrFigure, int JcurrFigure, int currFigure)
        {
            int dir = CurrentPlayer == 1 ? 1 : -1;
            switch (currFigure % 10)
            {
                case 6: // Пешка

                   // Для двойного хода пешки из дома
                    /*
                    do
                    {
                        if (InsideBorder(IcurrFigure + 2 * dir, JcurrFigure))
                        {
                            if (map[IcurrFigure + 2 * dir, JcurrFigure] == 0)
                            {
                                butts[IcurrFigure + 2 * dir, JcurrFigure].BackColor = Color.LightPink;
                                butts[IcurrFigure + 2 * dir, JcurrFigure].Enabled = true;
                            }
                        }
                    }
                    while (IcurrFigure =);
                    */

                    if (InsideBorder(IcurrFigure + 1 * dir, JcurrFigure))
                    {
                        if (map[IcurrFigure + 1 * dir, JcurrFigure] == 0)
                        {
                            butts[IcurrFigure + 1 * dir, JcurrFigure].BackColor = Color.LightPink;
                            butts[IcurrFigure + 1 * dir, JcurrFigure].Enabled = true;
                        }
                    }

                    if (InsideBorder(IcurrFigure + 1 * dir, JcurrFigure + 1))
                    {
                        if (map[IcurrFigure + 1 * dir, JcurrFigure + 1] != 0 && map[IcurrFigure + 1 * dir, JcurrFigure + 1] / 10 != CurrentPlayer)
                        {
                            butts[IcurrFigure + 1 * dir, JcurrFigure + 1].BackColor = Color.LightPink;
                            butts[IcurrFigure + 1 * dir, JcurrFigure + 1].Enabled = true;
                        }
                        if (InsideBorder(IcurrFigure + 1 * dir, JcurrFigure - 1))
                        {
                            if (map[IcurrFigure + 1 * dir, JcurrFigure - 1] != 0 && map[IcurrFigure + 1 * dir, JcurrFigure - 1] / 10 != CurrentPlayer)
                            {
                                butts[IcurrFigure + 1 * dir, JcurrFigure - 1].BackColor = Color.LightPink;
                                butts[IcurrFigure + 1 * dir, JcurrFigure - 1].Enabled = true;
                            }

                            //// 2. Добавить функцию превращения пешки в фигуру на последнем поле -
                            ///(через нумерацию кнопок || за 8 ходов пешки) ??
                            // 3. Добавить разметку
                           
                            //// 5. Проверка 
                            ///на Шах - (Если король находится на DeterminePath другого игрока, кроме хода пешки вперёд)
                            //// 6. Проверка на Мат - (-//- выше + у короля нет свободного хода + ни одна его фигура не может встать на или уйти с DeterminePath другого игрока)
                            //// 7. Проверка связки (игровой) - (см. выше)
                            //// 8. РОКИРОВКА
                            // 9. Поворот доски - (выделить область доски и rotate 180) ??
                            // 10. Добавить таймер
                            // 11. Добавить нотацию - (через разметку присваивать и записывать ход) ??
                            // 12. Взятие на проходе ????
                        }
                    }
                    break;
                case 5: // Ладья
                    ShowVerticalHorizontal(IcurrFigure, JcurrFigure);
                    break;
                case 3: // Слон
                    ShowDiagonal(IcurrFigure, JcurrFigure);
                    break;
                case 2: // Ферзь
                    ShowVerticalHorizontal(IcurrFigure, JcurrFigure);
                    ShowDiagonal(IcurrFigure, JcurrFigure);
                    break;
                case 1: // Король
                    ShowVerticalHorizontal(IcurrFigure, JcurrFigure, true);
                    ShowDiagonal(IcurrFigure, JcurrFigure, true);

                    break;
                case 4: // Конь
                    ShowHorseSteps(IcurrFigure, JcurrFigure);
                    break;
            }
        }

        public void ShowHorseSteps(int IcurrFigure, int JcurrFigure)
        {
            if (InsideBorder(IcurrFigure - 2, JcurrFigure + 1))
            {
                DeterminePath(IcurrFigure - 2, JcurrFigure + 1);
            }
            if (InsideBorder(IcurrFigure - 2, JcurrFigure - 1))
            {
                DeterminePath(IcurrFigure - 2, JcurrFigure - 1);
            }
            if (InsideBorder(IcurrFigure + 2, JcurrFigure + 1))
            {
                DeterminePath(IcurrFigure + 2, JcurrFigure + 1);
            }
            if (InsideBorder(IcurrFigure + 2, JcurrFigure - 1))
            {
                DeterminePath(IcurrFigure + 2, JcurrFigure - 1);
            }
            if (InsideBorder(IcurrFigure - 1, JcurrFigure + 2))
            {
                DeterminePath(IcurrFigure - 1, JcurrFigure + 2);
            }
            if (InsideBorder(IcurrFigure + 1, JcurrFigure + 2))
            {
                DeterminePath(IcurrFigure + 1, JcurrFigure + 2);
            }
            if (InsideBorder(IcurrFigure - 1, JcurrFigure - 2))
            {
                DeterminePath(IcurrFigure - 1, JcurrFigure - 2);
            }
            if (InsideBorder(IcurrFigure + 1, JcurrFigure - 2))
            {
                DeterminePath(IcurrFigure + 1, JcurrFigure - 2);
            }
        }

        public void DeactivateAllButtons()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    butts[i, j].Enabled = false;
                }
            }
        }

        public void ActivateAllButtons()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    butts[i, j].Enabled = true;
                }
            }
        }

        public void ShowDiagonal(int IcurrFigure, int JcurrFigure, bool isOneStep = false)
        {
            int j = JcurrFigure + 1;
            for (int i = IcurrFigure - 1; i >= 0; i--)
            {
                if (InsideBorder(i, j))
                {
                    if (!DeterminePath(i, j))
                        break;
                }
                if (j < 7)
                    j++;
                else break;

                if (isOneStep)
                    break;
            }

            j = JcurrFigure - 1;
            for (int i = IcurrFigure - 1; i >= 0; i--)
            {
                if (InsideBorder(i, j))
                {
                    if (!DeterminePath(i, j))
                        break;
                }
                if (j > 0)
                    j--;
                else break;

                if (isOneStep)
                    break;
            }

            j = JcurrFigure - 1;
            for (int i = IcurrFigure + 1; i < 8; i++)
            {
                if (InsideBorder(i, j))
                {
                    if (!DeterminePath(i, j))
                        break;
                }
                if (j > 0)
                    j--;
                else break;

                if (isOneStep)
                    break;
            }

            j = JcurrFigure + 1;
            for (int i = IcurrFigure + 1; i < 8; i++)
            {
                if (InsideBorder(i, j))
                {
                    if (!DeterminePath(i, j))
                        break;
                }
                if (j < 7)
                    j++;
                else break;

                if (isOneStep)
                    break;
            }
        }

        public void ShowVerticalHorizontal(int IcurrFigure, int JcurrFigure, bool isOneStep = false)
        {
            for (int i = IcurrFigure + 1; i < 8; i++)
            {
                if (InsideBorder(i, JcurrFigure))
                {
                    if (!DeterminePath(i, JcurrFigure))
                        break;
                }
                if (isOneStep)
                    break;
            }
            for (int i = IcurrFigure - 1; i >= 0; i--)
            {
                if (InsideBorder(i, JcurrFigure))
                {
                    if (!DeterminePath(i, JcurrFigure))
                        break;
                }
                if (isOneStep)
                    break;
            }
            for (int j = JcurrFigure + 1; j < 8; j++)
            {
                if (InsideBorder(IcurrFigure, j))
                {
                    if (!DeterminePath(IcurrFigure, j))
                        break;
                }
                if (isOneStep)
                    break;
            }
            for (int j = JcurrFigure - 1; j >= 0; j--)
            {
                if (InsideBorder(IcurrFigure, j))
                {
                    if (!DeterminePath(IcurrFigure, j))
                        break;
                }
                if (isOneStep)
                    break;
            }
        }

        public bool DeterminePath(int IcurrFigure, int j)
        {
            if (map[IcurrFigure, j] == 0)
            {
                butts[IcurrFigure, j].BackColor = Color.LightPink;
                butts[IcurrFigure, j].Enabled = true;
            }
            else
            {
                if (map[IcurrFigure, j] / 10 != CurrentPlayer)
                {
                    butts[IcurrFigure, j].BackColor = Color.LightPink;
                    butts[IcurrFigure, j].Enabled = true;
                }
                return false;
            }
            return true;
        }

        public bool InsideBorder(int ti, int tj)
        {
            if (ti >= 8 || tj >= 8 || ti < 0 || tj < 0)
                return false;
            return true;
        }

        public void CloseSteps()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((i + j + 1) % 2 == 0)
                    {
                        butts[i, j].BackColor = Color.LightGray;
                    }
                    else butts[i, j].BackColor = Color.DimGray;
                }
            }
        }
        public void SwitchPlayer()
        {
            if (CurrentPlayer == 1)
                CurrentPlayer = 2;
            else CurrentPlayer = 1;
        }
        
        private void Restbutton_Click_1(object sender, EventArgs e)
        {
            this.Controls.Clear();
            Init();
            CloseSteps();
        }
    }
}
