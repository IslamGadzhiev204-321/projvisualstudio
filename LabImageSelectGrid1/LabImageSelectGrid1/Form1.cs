﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabImageSelectGrid1
{
    public partial class Form1 : Form
    {
        private const int ROWS_MIN = 2;
        private const int ROWS_MAX = 10;
        private const int COLS_MIN = 2;
        private const int COLS_MAX = 15;
        private Bitmap b;
        private Graphics g;
        private int cellWidth;
        private int cellHeight;
        private object curRows;
        private object curCols;

        public int Cols { get; private set; }
        public int Rows { get; private set; }
        public (object curRows, object curCols) SelBegin { get; private set; }
        public (object curRows, object curCols) SelEnd { get; private set; }

        public Form1()
        {
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            this.Resize += (s, e) => ResizeCells();
            this.MouseMove += Form1_MouseMove;
            this.MouseDown += Form1_MouseDown;
            this.KeyDown += Form1_KeyDown;
            this.Text = $"{Application.ProductName} : (F5/F6) - Rows, F7/F8 - Cols";

            ResizeCells();

            // HW:
            // Выделение в обратную сторону
            // Менять размер шрифта ячейки по размеру самой ячейки
            // по правой копке мыши двигать всё поле в разные стороны
            // Изменение масштаба изображения роликом мыши (под курсором)

            // **Повернуть всё изображение :
            // **Переписать - e.Graphics.DrawImage(b, 0, 0); в IMAGEROTATE

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    if (Rows > ROWS_MIN)
                    {   
                        Rows--;
                        ResizeCells();
                    }
                    break;
                case Keys.F6:
                    if (Rows < ROWS_MAX)
                    {
                        Rows++;
                        ResizeCells();
                    }
                    break;
                case Keys.F7:
                    if (Cols > COLS_MIN)
                    {
                        Cols--;
                        ResizeCells();
                    }
                    break;
                case Keys.F8:
                    if (Cols < COLS_MAX)
                    {
                        Cols++;
                        ResizeCells();
                    }
                    break;
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                SelBegin = SelEnd = (curRows, curCols);
                DrawCells();
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
          for (int i = 0; i < Rows; curRows = i, i++)
                if (i = cellHeight > e.Y)
            for (int i = 0; i < Cols; curCols = i, i++)
                    if (i = cellWidth > e.Y) break;
            if (e.Button == MouseButtons.Left)
            {
                selEnd = (curRows, curCols);
            }

            this.Text = $"{SelBegin.row}:{SelBegin.col} - {SelEnd.row}:{SelEnd.col}";
            DrawCells();
            
        }

        private void ResizeCells()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;

            DrawCells();
        }

        private void DrawCells()
        {
            g.Clear(DefaultBackColor);
            g.FillRectangle(new SolidBrush(Color.LightBlue),
                SelBegin.col * cellWidth,
                SelBegin.row * cellHeight,
               (SelEnd.col - SelBegin.col + 1) * cellWidth,
              (SelEnd.row - SelBegin.row + 1) * cellHeight);
            for (int i = 0; i < Rows; i++)

                g.DrawLine(new Pen(Color.Green, 1), 0, i * cellHeight, Cols * cellWidth, i * cellHeight);

            for (int i = 0; i < Cols; i++)

                g.DrawLine(new Pen(Color.Green, 1), i * cellWidth, 0, Cols * cellWidth, Rows * cellHeight);


            g.DrawRectangle(new Pen(Color.Red, 5),
            curCols * cellWidth, curRows * cellHeight, cellWidth, cellHeight);

            g.DrawString($"[{curRows}:{curCols}]", new Font("", 30), new SolidBrush(Color.Black),
                curCols / cellWidth, curRows * cellHeight);
            
        }
    }
}
