﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabEvent
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();
            // способ с обращением к конструктору
            button2.Click += Button2_Click;
            // способ, вызывающий анонимный метож - делегат
            button3.Click += delegate
            {
                MessageBox.Show("Способ 3");
            };
            // способ для компактного прямого указания, через лямбда-выражение
            button4.Click += (s, e) => MessageBox.Show("Способ 4");
            //
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 2");
        }

        private void button1_Click(object sender, EventArgs e)
        {// способ простейший
            MessageBox.Show("Способ 1");
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
