﻿using System;
using System.Collections;

namespace LabQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue X = new Queue();
            X.Enqueue(123);
            X.Enqueue(99);
            X.Enqueue(new Queue());

            Console.WriteLine(X.Peek()); //посмотреть
            Console.WriteLine("---");
            while (X.Count > 0)
            {
                Console.WriteLine(X.Dequeue()); // извлечение из очереди
            }
        }
    }
}
