﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabImageScrollHor
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imBG;
        private Point startPoint;
        private int deltaX = 0; // for editing

        public Fm()
        {
            InitializeComponent();

           imBG = Properties.Resources.background1;
            this.Height = imBG.Height;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            this.MouseDown += (s, e) => startPoint = e.Location;
            this.MouseMove += Fm_MouseMove;
            this.KeyDown += Fm_KeyDown;

            // HW:
            // Add some new backgrounds in project and changigng mode
            // Get deal with "i < 3", choose background image with small width
            // Add acceleration for UpdateDeltaX(a) with "a"
            // Add some new images for motion with different valocities
            // .....
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    UpdateDeltaX(2);
                    break;
                case Keys.Right:
                    UpdateDeltaX(-2);
                    break;
            }
            this.Invalidate();
        }
        
        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - startPoint.X);
                startPoint = e.Location;
                this.Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX={deltaX}, v={v}";
            deltaX += v;
            if (deltaX > 0)
                deltaX -= imBG.Width;
            else
                if (deltaX < -imBG.Width)
                deltaX += imBG.Width;
        }

        private void UpdateBG()
        {
            // g.Clear(SystemColors.Control);
            for (int i = 0; i < 3; i++)
            {

            g.DrawImage(imBG, deltaX + i * imBG.Width, 0);

            }

        }
    }
}
