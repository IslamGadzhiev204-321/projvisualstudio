﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabPaint
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Point startPoint;
        private Pen myPen;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPen = new Pen(Color.Red);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            trPenWidth.Value = Convert.ToInt32(myPen.Width);
            trPenWidth.ValueChanged += (s, e) => myPen.Width = trPenWidth.Value;

            pxclr1.Click += (s, e) => myPen.Color = pxclr1.BackColor;
            pxclr2.Click += (s, e) => myPen.Color = pxclr2.BackColor;
            pxclr3.Click += (s, e) => myPen.Color = pxclr3.BackColor;
            pxclr4.Click += (s, e) => myPen.Color = pxclr4.BackColor;

            buCLear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                pxImage.Invalidate();
            };

            buSave.Click += BuSave_Click;
            buLoad.Click += BuLoad_Click;
            buAdd.Click += BuAdd_Click;
        }

        private void BuAdd_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1, 10),
                    rnd.Next(1, 10)
                   );
            }
            pxImage.Invalidate();
        }

        private void BuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();

            };
            
        }

        private void BuSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "*JPG Files(*.JPG)|*.JPG;";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
              b.Save(dialog.FileName);
            }
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }
    }
}
