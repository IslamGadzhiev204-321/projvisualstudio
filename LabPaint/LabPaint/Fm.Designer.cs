﻿namespace LabPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.pxclr4 = new System.Windows.Forms.PictureBox();
            this.pxclr3 = new System.Windows.Forms.PictureBox();
            this.pxclr2 = new System.Windows.Forms.PictureBox();
            this.pxclr1 = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.buCLear = new System.Windows.Forms.Button();
            this.buSave = new System.Windows.Forms.Button();
            this.buLoad = new System.Windows.Forms.Button();
            this.buAdd = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buAdd);
            this.panel1.Controls.Add(this.buLoad);
            this.panel1.Controls.Add(this.buSave);
            this.panel1.Controls.Add(this.buCLear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxclr4);
            this.panel1.Controls.Add(this.pxclr3);
            this.panel1.Controls.Add(this.pxclr2);
            this.panel1.Controls.Add(this.pxclr1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(193, 450);
            this.panel1.TabIndex = 0;
            // 
            // trPenWidth
            // 
            this.trPenWidth.Location = new System.Drawing.Point(0, 380);
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(174, 45);
            this.trPenWidth.TabIndex = 1;
            // 
            // pxclr4
            // 
            this.pxclr4.BackColor = System.Drawing.Color.Lime;
            this.pxclr4.Location = new System.Drawing.Point(94, 13);
            this.pxclr4.Name = "pxclr4";
            this.pxclr4.Size = new System.Drawing.Size(21, 22);
            this.pxclr4.TabIndex = 0;
            this.pxclr4.TabStop = false;
            // 
            // pxclr3
            // 
            this.pxclr3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pxclr3.Location = new System.Drawing.Point(67, 13);
            this.pxclr3.Name = "pxclr3";
            this.pxclr3.Size = new System.Drawing.Size(21, 22);
            this.pxclr3.TabIndex = 0;
            this.pxclr3.TabStop = false;
            // 
            // pxclr2
            // 
            this.pxclr2.BackColor = System.Drawing.Color.Red;
            this.pxclr2.Location = new System.Drawing.Point(40, 13);
            this.pxclr2.Name = "pxclr2";
            this.pxclr2.Size = new System.Drawing.Size(21, 22);
            this.pxclr2.TabIndex = 0;
            this.pxclr2.TabStop = false;
            // 
            // pxclr1
            // 
            this.pxclr1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.pxclr1.Location = new System.Drawing.Point(13, 13);
            this.pxclr1.Name = "pxclr1";
            this.pxclr1.Size = new System.Drawing.Size(21, 22);
            this.pxclr1.TabIndex = 0;
            this.pxclr1.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.Location = new System.Drawing.Point(198, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(602, 450);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // button1
            // 
            this.buCLear.Location = new System.Drawing.Point(13, 77);
            this.buCLear.Name = "button1";
            this.buCLear.Size = new System.Drawing.Size(127, 23);
            this.buCLear.TabIndex = 2;
            this.buCLear.Text = "Clear";
            this.buCLear.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buSave.Location = new System.Drawing.Point(13, 125);
            this.buSave.Name = "button2";
            this.buSave.Size = new System.Drawing.Size(127, 23);
            this.buSave.TabIndex = 3;
            this.buSave.Text = "Save to file";
            this.buSave.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buLoad.Location = new System.Drawing.Point(13, 172);
            this.buLoad.Name = "button3";
            this.buLoad.Size = new System.Drawing.Size(127, 23);
            this.buLoad.TabIndex = 4;
            this.buLoad.Text = "Load from file";
            this.buLoad.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.buAdd.Location = new System.Drawing.Point(13, 221);
            this.buAdd.Name = "button4";
            this.buAdd.Size = new System.Drawing.Size(127, 23);
            this.buAdd.TabIndex = 5;
            this.buAdd.Text = "Add random stars";
            this.buAdd.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "LabPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxclr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.PictureBox pxclr4;
        private System.Windows.Forms.PictureBox pxclr3;
        private System.Windows.Forms.PictureBox pxclr2;
        private System.Windows.Forms.PictureBox pxclr1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buAdd;
        private System.Windows.Forms.Button buLoad;
        private System.Windows.Forms.Button buSave;
        private System.Windows.Forms.Button buCLear;
    }
}

