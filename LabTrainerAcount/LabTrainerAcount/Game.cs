﻿using System;

namespace LabTrainerAcount
{
    internal class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string CodeText { get; private set; }
        public event EventHandler Change;

        private bool answerCorrect;

        internal void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        private void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(40);
            int xValue2 = rnd.Next(40);
            int xResult = xValue1 + xValue2;
            int xResultNew = xResult;

            if (rnd.Next(2) == 1)
            xResultNew += rnd.Next(1, 9) * (rnd.Next(2) == 1 ? 1 : -1);

            char znak;
            int s = rnd.Next(1, 5);
            switch (s)
            {
                case 1:
                    xResult = xValue1 + xValue2;
                    znak = '+';
                    break;
                case 2:
                    xResult = xValue1 * xValue2;
                    znak = '*';
                    break;
                case 3:
                    xResult = xValue1 - xValue2;
                    znak = '-';
                    break;
                case 4:
                    xResult = xValue1 / xValue2;
                    znak = '/';
                    break;
                default:
                    xResult = xValue1 + xValue2;
                    znak = '+';
                    break;
            }

            answerCorrect = xResult == xResultNew;
            CodeText = $"{xValue1} {znak} {xValue2} = {xResultNew}";
            Change?.Invoke(this, EventArgs.Empty);
        }
        public void DoAnswer(bool v)
        {
            if (v == answerCorrect)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue();
        }
    }
}