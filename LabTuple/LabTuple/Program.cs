﻿using System;

namespace LabTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            var x0 = 5;
            int x00 = 5;

            //(1) Прямой способ, без имён
            var x1 = (2, 4);
            (int, int) x11 = (2, 4);

            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine("---");

            //(2) Название полей кортежей через объявления
            (int min, int max) x2 = (2, 4);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);
            Console.WriteLine("---");

            //(3) Название полей кортежей через инициализацию
            var x3 = (min: 2, max: 4);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);
            Console.WriteLine("---");

            //(4) Отдельные переменные для каждого поля кортежа
            var (min, max) = (2, 4);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.WriteLine("---");

            //(5) Получение кортежа
            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine("---");

            //(6)  Получение кортежа с именами
            var x6 = GetX6();
            Console.WriteLine(x6.aaa);
            Console.WriteLine(x6.bbb);
            Console.WriteLine("---");

            //(7) Передача кортежа в качестве параметра метода
            var x7 = GetX7((4, 5), 6);
            Console.WriteLine(x7.a);
            Console.WriteLine(x7.b);
        }

        private static (int a, int b) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);

        private static (int aaa, int bbb) GetX6() => (3, 6);

        private static (int, int) GetX5() => (3, 6); // короткая форма
        }
        //private static (int, int) GetX5()
        //{
        //    return (3, 6);
        //}
    
}
